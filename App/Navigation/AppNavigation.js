import { createAppContainer } from 'react-navigation'
import DaftarSubmisiDariDbScreen from '../Containers/DaftarSubmisiDariDbScreen'
import BackendMenuScreen from '../Containers/BackendMenuScreen'
import LihatSubmisiScreen from '../Containers/LihatSubmisiScreen'
import LihatSummarySubmisiScreen from '../Containers/LihatSummarySubmisiScreen'
import DaftarSubmisiScreen from '../Containers/DaftarSubmisiScreen'
import IsiFormScreen from '../Containers/IsiFormScreen'
import PilihMenuScreen from '../Containers/PilihMenuScreen' 
import { createStackNavigator } from 'react-navigation-stack'; 

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = createStackNavigator({
  DaftarSubmisiDariDbScreen: { screen: DaftarSubmisiDariDbScreen },
  BackendMenuScreen: { screen: BackendMenuScreen },
  LihatSubmisiScreen: { screen: LihatSubmisiScreen },
  LihatSummarySubmisiScreen: { screen: LihatSummarySubmisiScreen },
  DaftarSubmisiScreen: { screen: DaftarSubmisiScreen },
  IsiFormScreen: { screen: IsiFormScreen },
  PilihMenuScreen: { screen: PilihMenuScreen }, 
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'PilihMenuScreen', // <--- 
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default createAppContainer(PrimaryNav)
