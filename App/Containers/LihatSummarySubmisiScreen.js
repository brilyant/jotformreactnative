import React, { Component } from 'react'
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Form, Item, Label, Input } from 'native-base'
import { connect } from 'react-redux' 
import Divider from 'react-native-divider'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/LihatSummarySubmisiScreenStyle'
import StaticVar from '../Config/StaticVar'
import FooterProcessButton from '../Components/FooterProcessButton'

class LihatSubmisiScreen extends Component {
  render () {
    const item = this.props.navigation.getParam('item')
    const showButtonProcess = this.props.navigation.getParam('showButtonProcess')


    //Secara default, field dari response JOTFORM API kurang lebih seperti ini :
    /*
    {
      "19": {
        "name": "jabatan",
        "order": "20",
        "text": "Jabatan",
        "type": "control_dropdown"
      },
      "24": {
        "name": "namaDirektur",
        "order": "19",
        "sublabels": "{\"prefix\":\"Prefix\",\"first\":\"Nama Depan\",\"middle\":\"Middle Name\",\"last\":\"Nama Belakang\",\"suffix\":\"Suffix\"}",
        "text": "Nama Direktur",
        "type": "control_fullname"
      },
      "28": {
        "answer": "J"
      }
    }

    Kita akan Convert dari yg keynya nomor ("19", "24", "28") menjadi by 'name' seperti di bawah :
    {
      jabatan: {
        name: 'jabatan',
        order: '20',
        text: 'Jabatan',
        type: 'control_dropdown',
        answer: ''
        },
      namaDirektur: {
        name: 'namaDirektur',
        order: '19',
        sublabels: '{"prefix":"Prefix","first":"Nama Depan","middle":"Middle Name","last":"Nama Belakang","suffix":"Suffix"}',
        text: 'Nama Direktur',
        type: 'control_fullname',
        answer: ''
      },
      '28': { answer: 'J' },
    }

    //Catatan: pada key 28, karena isinya tidak memiliki name, maka kita akan tetap biarkan keynya seperti apa adanya..

    Dengan struktur key by field name seperti diatas, kita bisa memanggil nilai field dengan seperti ini : variablePenampungKonversi['jabatan'].answer
     
    */

    //Mulai Konversi
    let convertJSONByFieldName = {}
    Object.keys(item.answers).forEach(function (key) {
      const newKey = item.answers[key].name ? item.answers[key].name : key //ganti key dengan field name, tidak ada field name ?, biarkan dengan existing key
      //simpan lg isi data ke key baru, dan tambah field 'answer' bila existing tidak memiliki field tersbut
      convertJSONByFieldName[newKey] = {
        ...item.answers[key], 
        answer: item.answers[key].answer ? item.answers[key].answer : '',
        prettyFormat: item.answers[key].prettyFormat ? item.answers[key].prettyFormat : ''
      } 
    });
     
    console.log(JSON.stringify(item))
 
    return (
      <Container>
        <Header noLeft>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Lihat Submisi</Title>
          </Body>
          <Right />
        </Header>
        <Content> 

          <Form> 
            <Item stackedLabel>
              <Label>Waktu Submisi</Label>
              <Input disabled value={item.created_at} />
            </Item>
            <Item stackedLabel>
              <Label>IP Submitter</Label>
              <Input disabled value={item.ip} />
            </Item>
            <Divider>Summary</Divider>
            
            <Item stackedLabel>
              <Label>Nama</Label>
              <Input disabled value={convertJSONByFieldName['nama108'].prettyFormat} />
            </Item>
            
            <Item stackedLabel>
              <Label>Nomor KTP</Label>
              <Input disabled value={convertJSONByFieldName['nomorKtp'].answer} />
            </Item>

            <Item stackedLabel>
              <Label>Nomor Telepon</Label>
              <Input disabled value={convertJSONByFieldName['nomorTelepon'].answer} />
            </Item>

            <Item stackedLabel>
              <Label>Tempat Lahir</Label>
              <Input disabled value={convertJSONByFieldName['tempatLahir'].answer} />
            </Item>

            <Item stackedLabel>
              <Label>Tanggal Lahir</Label>
              <Input disabled value={convertJSONByFieldName['tanggalLahir142'].prettyFormat} />
            </Item>

            <Item stackedLabel>
              <Label>Pekerjaan</Label>
              <Input disabled value={convertJSONByFieldName['pekerjaan'].answer} />
            </Item>  

            <Item stackedLabel>
              <Label>Sifat perjalanan</Label>
              <Input disabled value={convertJSONByFieldName['sifatPerjalanan139'].answer} />
            </Item>

            <Item stackedLabel>
              <Label>Alasan Keluar/Masuk Papua</Label>
              <Input disabled value={convertJSONByFieldName['alasanKeluarmasuk'].answer} />
            </Item>
            
            <Item stackedLabel>
              <Label>Keterangan Alasan Keluar Papua</Label>
              <Input disabled value={convertJSONByFieldName['keteranganAlasan'].answer} />
            </Item>

            <Item stackedLabel>
              <Label>Hasil PCR</Label>
              <Input disabled value={convertJSONByFieldName['hasilPcr'].answer} />
            </Item>

          </Form> 

        </Content> 
        <FooterProcessButton showButton={showButtonProcess} submissionid={item.id}/>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LihatSubmisiScreen)
