import { StackActions, SwitchActions, NavigationActions } from 'react-navigation'
import AppNavigation from '../Navigation/AppNavigation'

const GOBACK = (state) => {   
  const popAction = StackActions.pop({
    n: 1, //back 1 saja
  }) 
  return AppNavigation.router.getStateForAction(popAction, state) 
}  
 

export const reducer = (state, action) => {
  // console.log('action', action)
  switch (action.type) { 
  case 'SUBMISSION_APPROVE_SUCCESS': //bila action SUBMISSION_APPROVE_SUCCESS (submissionApproveSuccess) tertangkap jalankan GOBACK 
    return GOBACK(state)  
  }
  

  const newState = AppNavigation.router.getStateForAction(action, state)
  return newState || state
} 
 