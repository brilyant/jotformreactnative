
import { Alert } from 'react-native'

export default(title, desc, positiveResponse, negativeResponse, positiveText = 'Ok', negativeText = 'Batal') => {
  Alert.alert(
    title,
    desc,
    [ 
      {
        text: negativeText,
        onPress: () => negativeResponse ? negativeResponse() : console.log('Cancel Pressed'),
        style: 'cancel'
      },
      { text: positiveText, onPress: () => positiveResponse ? positiveResponse() : console.log('OK Pressed') }
    ],
    { cancelable: true }
  )
}

