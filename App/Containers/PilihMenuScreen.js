import React, { Component } from 'react'
// import { ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/PilihMenuScreenStyle'

class PilihMenuScreen extends Component {
  render () {
    return (
      <Container>
        <Header noLeft>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Test UI Jotform</Title>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
          <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
            <Button 
              style={{marginHorizontal: 35, marginVertical: 10 }} 
              block
              onPress={() => this.props.navigation.navigate('IsiFormScreen')}
            >
              <Text>Isi Form</Text>
            </Button>
            <Button 
              style={{marginHorizontal: 35, marginVertical: 10 }} 
              block
              onPress={() => this.props.navigation.navigate('DaftarSubmisiScreen')}
            >
              <Text>Tampilkan Submisi</Text>
            </Button>
          </Content>
          <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
            <Text style={{ textAlign: 'center'}}>Sample Approve / dan list submisi ter-approve ada di menu backend ini (di bawah)</Text>
            <Button 
              style={{marginHorizontal: 35, marginVertical: 10 }} 
              block
              danger
              onPress={() => this.props.navigation.navigate('BackendMenuScreen')}
            >
              <Text>Backend</Text>
            </Button>
          </Content>
        </Content> 
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PilihMenuScreen)
