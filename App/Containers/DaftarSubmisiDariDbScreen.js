import React, { Component } from 'react'
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text, List, ListItem, View, Subtitle } from 'native-base'
import { connect } from 'react-redux' 
import {Alert} from 'react-native'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import GetDataFromDbActions from '../Redux/GetDataFromDbRedux'

// Styles
import styles from './Styles/DaftarSubmisiDariDbScreenStyle'
import { FlatList, RefreshControl } from 'react-native'

class DaftarSubmisiDariDbScreen extends Component {

  componentDidMount() {
    const approved = this.props.navigation.getParam('approved') // ambil nilai param 'approved' dari screen sebelumnya
    this.props.getDataFromDbRequest({ approved: approved }) // tampilkan data sesuai filter
  }
 
  renderItem ({item}) {
    const approved = this.props.navigation.getParam('approved')
    return(
      <List>
        <ListItem last onPress={() => {
          //kasih percabangan, mau summary atau full jotform ui
          Alert.alert(
            'Pilih Tampilan',
            'Ingin di tampilan secara summary / keseluruhan ?',
            [
              {
                text: 'Batal',
                onPress: () => console.log('Batal'),
                style: 'cancel'
              },
              {
                text: 'Summary !',
                onPress: () => this.props.navigation.navigate('LihatSummarySubmisiScreen', {item: item, showButtonProcess: (approved) ? false : true }), //bila sudah approved showButtonProcess = false. alias jangan tampilkan button proses
                style: 'default'
              },
              { text: 'Keseluruhan !', onPress: () => this.props.navigation.navigate('LihatSubmisiScreen', {id: item.id, showButtonProcess: true}) }
            ],
            { cancelable: false }
          );
        }}>
          <Body>
            <Text>{`ID Submisi: ${item.id}`}</Text>
            <Text>{`Tanggal: ${item.created_at}`}</Text>
            <Text note>{`IP Submitter: ${item.ip}`}</Text>
          </Body>
          
        </ListItem> 
      </List>
    )
  }
   
  render () {
    return (
      <Container>
        <Header noLeft>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Subtitle>Daftar Submisi Dari DB Kita</Subtitle>
          </Body>
          <Right />
        </Header>
        <View style={{ flex: 1, justifyContent: 'center' }}> 
          <View style={{ flex: 0.15, backgroundColor: '#DCDCDC', borderBottomColor: 'black', borderBottomWidth: 1 }}>
            <Text style={{ textAlign: 'center' }}>Data yg ditampilkan dibawah</Text>
            <Text style={{ textAlign: 'center' }}>Adalah data yg berasal dari <Text style={{fontWeight: 'bold'}}>database kita</Text>, bukan jotform</Text>
          </View>
          <View style={{ flex: 0.85}}>
            <FlatList 
              keyboardShouldPersistTaps="always"
              refreshControl={
                <RefreshControl refreshing={this.props.getdatafromdb.fetching === true} onRefresh={() => {
                  const approved = this.props.navigation.getParam('approved') // ambil nilai param 'approved' dari screen sebelumnya
                  this.props.getDataFromDbRequest({ approved: approved }) // tampilkan data sesuai filter
                } } />
              }
              data={ this.props.getdatafromdb.payload || []}
              ListEmptyComponent={() => (
                <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
                  <Text style={{ textAlign: 'center' }}>Data di database kosong</Text>
                </Content>
              )}
              contentContainerStyle={{ flexGrow: 1 }}
              renderItem={this.renderItem.bind(this)}

            /> 
          </View>
        </View> 
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    getdatafromdb: state.getdatafromdb
  }
}
  
const mapDispatchToProps = (dispatch) => {
  return {
    getDataFromDbRequest: (data) => dispatch(GetDataFromDbActions.getDataFromDbRequest(data))
  }

}
export default connect(mapStateToProps, mapDispatchToProps)(DaftarSubmisiDariDbScreen)