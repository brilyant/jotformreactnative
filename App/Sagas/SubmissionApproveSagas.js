/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the Infinite Red Slack channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put, select } from 'redux-saga/effects'
import SubmissionApproveActions from '../Redux/SubmissionApproveRedux'
import GetDataFromDbActions, { GetDataFromDbSelectors } from '../Redux/GetDataFromDbRedux'
import { Toast } from 'native-base'

export function * submissionApproveRequest (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(SubmissionApproveSelectors.getData)
  // make the call to the api
  const response = yield call(api.submissionApproveRequest, data)

  // success?
  if (response.ok) {
    //bila proses berhasil, maka kita akan langsung hilangkan data dalam list payload 'GetDataFromDbRedux', karena dengan asumsi data yg sudah di approve tidak di munculkan lagi di list tersebut, melainkan pindah ke screen Daftar Data Approved

    //pertama kita dapatkan existing data yg ada pada payload redux 'GetDataFromDbRedux'
    const currentData = yield select(GetDataFromDbSelectors.getPayload)

    const newListData = [
      ...currentData.filter(x => x.id !== data.id) //keluarkan data dengan id yg sama dengan yg kita proses dari array currentData, dan langsung tampung ke var newListData
    ]

    //Masukan kembali seluruh array yg ada pada newListData, ke payload 'GetDataFromDbRedux', lewat action 'GetDataFromDbActions.getDataFromDbSuccess'
    yield put(GetDataFromDbActions.getDataFromDbSuccess(newListData))

    //jalankan aksi submissionApproveSuccess, ini akan mentrigger navigasi go Back (lihat di file: ..\App\Redux\NavigationRedux.js)
    yield put(SubmissionApproveActions.submissionApproveSuccess(response.data))

    Toast.show({
      text: 'Berhasil Approve',
      type: 'success'
    })

  } else {
    console.log(JSON.stringify(response))
    Toast.show({
      text: 'Gagal Approve, cek log',
      type: 'danger'
    })    
    yield put(SubmissionApproveActions.submissionApproveFailure())
  }
}
