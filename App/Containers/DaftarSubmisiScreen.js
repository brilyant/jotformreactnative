import React, { Component } from 'react'
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text, List, ListItem, View, Subtitle } from 'native-base'
import { connect } from 'react-redux' 
// Add Actions - replace 'Your' with whatever your reducer is called :)
import JotFormSubmissionActions from '../Redux/JotFormSubmissionRedux'

// Styles
import styles from './Styles/DaftarSubmisiScreenStyle'
import { FlatList, RefreshControl } from 'react-native'

class DaftarSubmisiScreen extends Component {

  componentDidMount() {
    this.props.jotFormSubmissionRequest()
  }
 
  renderItem ({item}) {
    return(
      <List>
        <ListItem last onPress={() => this.props.navigation.navigate('LihatSubmisiScreen', {id: item.id})}>
          <Body>
            <Text>{`ID Submisi: ${item.id}`}</Text>
            <Text>{`Tanggal: ${item.created_at}`}</Text>
            <Text note>{`IP Submitter: ${item.ip}`}</Text>
          </Body>
          
        </ListItem> 
      </List>
    )
  }
   
  render () {
    return (
      <Container>
        <Header noLeft>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Subtitle>Daftar Submisi dr jotform</Subtitle>
          </Body>
          <Right />
        </Header>
        <View contentContainerStyle={{ flex: 1, justifyContent: 'center' }}> 
          <FlatList
            keyboardShouldPersistTaps="always"
            refreshControl={
              <RefreshControl refreshing={this.props.jotformsubmission.fetching === true} onRefresh={() => this.props.jotFormSubmissionRequest() } />
            }
            data={ this.props.jotformsubmission.payload || []}
            contentContainerStyle={{ flexGrow: 1 }}
            renderItem={this.renderItem.bind(this)}

          />
        </View> 
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    jotformsubmission: state.jotformsubmission
  }
}
  
const mapDispatchToProps = (dispatch) => {
  return {
    jotFormSubmissionRequest: (data) => dispatch(JotFormSubmissionActions.jotFormSubmissionRequest(data))
  }

}
export default connect(mapStateToProps, mapDispatchToProps)(DaftarSubmisiScreen)