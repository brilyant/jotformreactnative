import React, { Component } from 'react'
// import { ScrollView, Text, KeyboardAvoidingView } from 'react-native'
import { Container, Header, Content, Button, Left, Right, Body, Icon, Text, Subtitle } from 'native-base'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import PullJotformDataActions from '../Redux/PullJotformDataRedux'


// Styles
import styles from './Styles/BackendMenuScreenStyle'
import { ActivityIndicator } from 'react-native'
import AlertYesNo from '../Lib/AlertYesNo'

class BackendMenuScreen extends Component {
  render () {
    console.log(this.props.pulljotformdata)
    const { fetching } = this.props.pulljotformdata
    return (
      <Container>
        <Header noLeft>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Subtitle>Test UI Jotform (Backend)</Subtitle>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
          <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
            <Text style={{textAlign: 'center'}}>
              Harap dipastikan bahwa server backend sudah up.
            </Text>
            <Text style={{textAlign: 'center'}}>
              Ganti Url Backend di :
            </Text>
            <Text style={{textAlign: 'center'}}>
              {'...\\JotformReactNative\\App\\Config\\StaticVar.js -> URL_API_KITA'}
            </Text>
          </Content>
          <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
            <Button 
              disabled={fetching === true}
              style={{marginHorizontal: 35, marginVertical: 10 }} 
              block
              info
              onPress={() => {
                AlertYesNo('Konfirmasi Tarik Data', 'Proses ini akan menarik Data dari Jotform ke Database Kita.', () => {
                  this.props.pullJotformDataRequest()
                }, undefined, 'Tarik !') 
              }}
            >
              {
                (fetching === true) ? 
                  <ActivityIndicator size="small" color="red" /> :
                  <Text uppercase={false}>Tarik Data Jotform ke DB kita</Text>
              }
            </Button>
            <Button 
              disabled={fetching === true}
              style={{marginHorizontal: 35, marginVertical: 10 }} 
              block 
              onPress={() => this.props.navigation.navigate('DaftarSubmisiDariDbScreen', {approved: false})}
            >
              {
                (fetching === true) ? 
                  <ActivityIndicator size="small" color="red" /> : 
                  <Text style={{ textAlign: 'center'}} uppercase={false}>Tampilkan Submisi dari DB Kita & Proses Approval</Text> 
              }
            </Button>

            <Button 
              disabled={fetching === true}
              style={{marginHorizontal: 35, marginVertical: 10 }} 
              block 
              danger
              onPress={() => this.props.navigation.navigate('DaftarSubmisiDariDbScreen', {approved: true})}
            >
              {
                (fetching === true) ? 
                  <ActivityIndicator size="small" color="red" /> : 
                  <Text style={{ textAlign: 'center'}} uppercase={false}>Tampilkan Submisi dari DB Kita yg sudah APPROVED</Text> 
              }
            </Button>
          </Content>
        </Content> 
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    pulljotformdata: state.pulljotformdata
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    pullJotformDataRequest: (data) => dispatch(PullJotformDataActions.pullJotformDataRequest(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BackendMenuScreen)
