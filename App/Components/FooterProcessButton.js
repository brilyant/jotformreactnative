import React, { Component } from 'react'
import { connect } from 'react-redux' 
import { View, ActivityIndicator } from 'react-native'
import { Text, Footer, FooterTab, Button } from 'native-base'
import styles from './Styles/FooterProcessButtonStyle'
import SubmissionApproveActions from '../Redux/SubmissionApproveRedux'
import { withNavigation } from 'react-navigation'
import AlertYesNo from '../Lib/AlertYesNo'

class FooterProcessButton extends React.PureComponent {
  render () {
    if (this.props.showButton) {
      return (
        <Footer>
          {
            (this.props.submissionapprove.fetching === true) ? 
              <ActivityIndicator size="small" />
              :
              <FooterTab>
                <Button full onPress={() => {
                  AlertYesNo('Approval', 'Yakin Approve Data ?', () => {
                    this.props.submissionApproveRequest({ id: this.props.submissionid })
                  })
                }}>
                  <Text>Terima</Text>
                </Button>
                <Button danger full onPress={() => this.props.navigation.goBack(null)}>
                  <Text>Tolak</Text>
                </Button>
              </FooterTab>
          }
        </Footer> 
      )
    }
    return null
  }
}


const mapStateToProps = (state) => {
  return {
    submissionapprove: state.submissionapprove
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    submissionApproveRequest: (data) => dispatch(SubmissionApproveActions.submissionApproveRequest(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(FooterProcessButton))
