import React, { Component } from 'react'
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text } from 'native-base'
import { connect } from 'react-redux'
import { WebView } from 'react-native-webview'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/IsiFormScreenStyle'

class IsiFormScreen extends Component {
  render () {
    return (
      <Container>
        <Header noLeft>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Input Data</Title>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}> 

          <WebView
            ref={ x => this.webview = x}
            style={{ flex: 1 }}
            originWhitelist={['*']} 
            domStorageEnabled={true} 
            startInLoadingState={true}
            javaScriptEnabled={true}
            source={{uri: 'https://form.jotform.com/201671570436049' }}
          />


        </Content> 
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(IsiFormScreen)
