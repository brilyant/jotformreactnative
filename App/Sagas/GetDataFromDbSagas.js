/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the Infinite Red Slack channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import GetDataFromDbActions from '../Redux/GetDataFromDbRedux'
import { Toast } from 'native-base'
// import { GetDataFromDbSelectors } from '../Redux/GetDataFromDbRedux'

export function * getDataFromDbRequest (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(GetDataFromDbSelectors.getData)
  // make the call to the api
  const response = yield call(api.getDataFromDbRequest, data)

  // success?
  // success?
  if (response.ok) { 
    yield put(GetDataFromDbActions.getDataFromDbSuccess(response.data))
  } else {
    Toast.show({
      text: 'gagal terhubung ke server',
      type: 'danger'
    })
    yield put(GetDataFromDbActions.getDataFromDbFailure())
  }
}
