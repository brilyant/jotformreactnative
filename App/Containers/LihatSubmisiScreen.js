import React, { Component } from 'react'
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon } from 'native-base'
import { connect } from 'react-redux'
import { WebView } from 'react-native-webview'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/LihatSubmisiScreenStyle'
import StaticVar from '../Config/StaticVar'
import FooterProcessButton from '../Components/FooterProcessButton'

class LihatSubmisiScreen extends Component {
  render () {
    const id = this.props.navigation.getParam('id')
    const showButtonProcess = this.props.navigation.getParam('showButtonProcess')

    const url = `https://www.jotform.com/submission/${id}?apiKey=${StaticVar.apiKey}`

    console.log('url', url)
    return (
      <Container>
        <Header noLeft>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Lihat Submisi</Title>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}> 

          <WebView
            ref={ x => this.webview = x}
            style={{ flex: 1 }}
            originWhitelist={['*']} 
            domStorageEnabled={true} 
            startInLoadingState={true}
            javaScriptEnabled={true}
            source={{uri: url }}
          />
        </Content> 
        <FooterProcessButton showButton={showButtonProcess} submissionid={id}/>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LihatSubmisiScreen)
